// Templates used, should be in another file

const statsItemTpl = `<div class="user" data-username="{{name}}">
  <div class="user-name">
    {{name}}
  </div>
  <div class="progress">
    <div class="progress-bar"></div>
  </div>
</div>`;
const timeoutTpl =
  'The next race starts in <span class="timeout">{{timeout}}</span> s';
const gameAlready =
  'Wait <span class="timeout">{{timeout}}</span> s, until the current race will be finished';
const gameEndTimeoutTpl =
  '<span class="min">3</span> min <span class="sec">33</span> s';

window.onload = () => {
  const messageElem = document.querySelector(".message"),
    statsElem = document.querySelector(".stats"),
    textElem = document.querySelector(".text"),
    timeoutElem = document.querySelector(".countdown");

  const jwt = localStorage.getItem("jwt");

  const commentator = new Commentator(
    document.querySelector(".commentator .comments")
  );

  let raceText, raceProgress, currentChar;

  if (!jwt) {
    location.replace("/login");
  } else {
    const socket = io.connect("http://localhost:3000");

    socket.emit("userConnected", { token: jwt });

    socket.on("authFailed", () => {
      messageElem.innerHTML =
        "Authentification failed. You will be redirected in 2 seconds...";
      setTimeout(() => {
        window.location = "/login";
      }, 2000);
    });

    // when you are joined
    socket.on("youAreJoined", ({ stats }) => {
      updateStats(stats);
      const userCount = Object.keys(stats).length;
      if (userCount == 1) {
        messageElem.innerHTML = "Waiting for someone to race with...";
      }
    });

    socket.on("newUserJoined", ({ stats }) => {
      updateStats(stats);
    });

    socket.on("timeoutUpdate", ({ countdownTime }) => {
      messageElem.innerHTML = timeoutTpl;
      createCountdown({
        secondsElem: messageElem.querySelector(".timeout"),
        countdownTime
      }); // Time when to stop countdown in world date time format
    });

    socket.on("gameIsAlreadyStarted", ({ endGameTime }) => {
      messageElem.innerHTML = gameAlready;
      createCountdown({
        secondsElem: messageElem.querySelector(".timeout"),
        countdownTime: endGameTime,
        callback: () => window.location.reload()
      });
    });

    socket.on("setTextId", async ({ textId }) => {
      raceText = await fetch(`/text/${textId}`, {
        headers: {
          Authorization: `Bearer ${jwt}`
        }
      })
        .then(res => {
          if (res.ok) {
            return res.text();
          } else {
            return Promise.reject(Error("Failed to load"));
          }
        })
        .catch(error => {
          throw error;
        });
    });

    socket.on("startGame", ({ endGameTime }) => {
      startGame();
      timeoutElem.querySelector(".time").innerHTML = gameEndTimeoutTpl;
      timeoutElem.style.visibility = "visible";

      createCountdown({
        minutesElem: timeoutElem.querySelector(".min"),
        secondsElem: timeoutElem.querySelector(".sec"),
        countdownTime: endGameTime
      });
    });

    socket.on("updateProgress", ({ stats, userName }) => {
      updateProgress(stats, userName);
    });

    socket.on("comment", ({ text }) => {
      commentator.addMessage(text);
    });

    function startGame() {
      messageElem.innerHTML = "Game started";
      raceProgress = 0;

      updateText(raceProgress);
      document.addEventListener("keypress", onKeyPress);

      socket.on("endGame", () => {
        endGame("Time is out. Race is finished");
      });

      function endGame(message) {
        messageElem.innerHTML = message;
        document.removeEventListener("keypress", onKeyPress);
      }

      function onKeyPress(e) {
        const char = getChar(e);
        if (char == currentChar) {
          raceProgress = raceProgress + 1;
          updateText(raceProgress);
          if (raceProgress == raceText.length) {
            endGame("You finished!!!");
          }
          socket.emit("increment", { token: jwt });
        } else {
          textElem.querySelector("strong").classList.add("error");
        }
      }
    }

    function updateProgress(stats, userName) {
      Object.keys(stats)
        .sort((b, a) => {
          console.log(a, b);
          let compare = stats[a].progress - stats[b].progress;
          if (compare == 0) {
            compare = stats[a].time - stats[b].time;
          }
          return compare;
        })
        .forEach((name, index) => {
          if (stats[name].status == -1) {
            statsElem
              .querySelector(`[data-username=${name}]`)
              .classList.add("crashed");
          }
          statsElem.querySelector(
            `[data-username=${name}] .progress-bar`
          ).style.width = (stats[name].progress / raceText.length) * 100 + "%";
          statsElem.querySelector(
            `[data-username=${name}]`
          ).style.order = index;
        });

      // if multiple windows of same user opened we need to sync
      // entered letters
      if (userName && raceProgress !== stats[userName].progress) {
        raceProgress = stats[userName].progress;
        updateText(raceProgress);
      }
    }
    function updateText(raceProgress) {
      currentChar = raceText.substr(raceProgress, 1);

      textElem.innerHTML =
        "<span>" +
        raceText.substr(0, raceProgress) +
        "</span><strong>" +
        currentChar +
        "</strong>" +
        raceText.substr(raceProgress + 1);
    }
  }

  function updateStats(stats) {
    const items = Object.keys(stats).map(name => {
      return statsItemTpl.replace(new RegExp("{{name}}", "g"), name);
    });
    statsElem.innerHTML = items.join("");
    statsElem.classList.add("visible");
  }
};

function createCountdown(params) {
  const { minutesElem, secondsElem, countdownTime, callback } = params;
  updateCountdown();
  const interval = setInterval(() => {
    updateCountdown();
  }, 1000);

  function updateCountdown() {
    const now = new Date().getTime();
    const timeout = Math.floor((countdownTime - now) / 1000);
    if (timeout <= 0 && interval) {
      clearInterval(interval);
      if (callback && typeof callback === "function") {
        callback();
      }
    }
    if (minutesElem) {
      minutesElem.innerHTML = Math.floor(timeout / 60);
      secondsElem.innerHTML = timeout % 60;
    } else {
      secondsElem.innerHTML = timeout;
    }
    return timeout;
  }
}

function getChar(event) {
  if (event.which == null) {
    // IE
    if (event.keyCode < 32) return null; // спец. символ
    return String.fromCharCode(event.keyCode);
  }

  if (event.which != 0 && event.charCode != 0) {
    // все кроме IE
    if (event.which < 32) return null; // спец. символ
    return String.fromCharCode(event.which); // остальные
  }

  return null; // спец. символ
}

class Commentator {
  constructor(textELem) {
    this.textELem = textELem;
  }
  addMessage(messageText) {
    const messageHTML = `<p>${messageText}</p>`;
    this.textELem.insertAdjacentHTML("beforeend", messageHTML);
    this.textELem.scrollTop = this.textELem.scrollHeight;
  }
}
