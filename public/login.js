window.onload = () => {
  const loginField = document.querySelector("#login");
  const passField = document.querySelector("#password");
  const loginButton = document.querySelector("#submit");

  loginButton.addEventListener("click", e => {
    e.preventDefault();
    fetch("/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        login: loginField.value,
        password: passField.value
      })
    })
      .then(res => {
        res.json().then(body => {
          console.log(body);
          if (body.auth) {
            localStorage.setItem("jwt", body.token);
            location.replace("/race");
          } else {
            console.log("auth.failed");
          }
        });
      })
      .catch(err => {
        console.log("request went wrong");
      });
    return false;
  });
};
