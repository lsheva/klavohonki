const passport = require("passport");
const JWTStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const users = require("./users");

const options = {
  secretOrKey: "someSecret",
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

passport.use(
  new JWTStrategy(options, async (payload, done) => {
    try {
      const user = users.find(userFromDB => {
        if (userFromDB.login === payload.login) {
          return userFromDB;
        }
      });
      return user
        ? done(null, user)
        : done({ status: 401, message: "Invalid token" });
    } catch (arr) {
      return done(arr);
    }
  })
);
