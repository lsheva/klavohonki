const _ = require("lodash");

class Commentator {
  constructor(socket) {
    this.socket = socket;
    this.gibber();
  }

  gibber() {
    if (this.gibberTimeout) {
      clearTimeout(this.gibberTimeout);
    }
    this.gibberTimeout = setTimeout(
      () => this.say("Gibber"),
      Math.floor(Math.random() * 15000 + 15000)
    );
  }

  say(type, opts) {
    const message = new MessageProxy(type, opts);
    this.socket.emit("comment", { text: message.getText() });
    this.socket.broadcast.emit("comment", { text: message.getText() });
    this.gibber();
  }
}

// ***************** //
//  Factory pattern  //
// _________________ //

class Message {
  constructor(opts) {
    if (opts && opts.text) {
      this.text = text;
    }
    this.strings = [];
  }
  getRandomString() {
    const index = Math.floor(this.strings.length * Math.random());
    return this.strings[index];
  }
  getText() {
    return this.text;
  }
}

class Introduce extends Message {
  constructor(opts) {
    super(opts);
    this.strings = [
      "На вулиці зараз трохи пасмурно, але на Львів Арена зараз просто чудова атмосфера: двигуни гарчать, глядачі посміхаються а гонщики ледь помітно нервують та готуюуть своїх залізних конів до заїзду. А коментувати це все дійстово Вам буду я, Аполон Аполонович і я радий вас вітати зі словами Доброго Вам дня панове!",
      "Щирі вітання хлопці та дівчата, я буду не багатослівним, тож вперед, приготуємося до змагань",
      "Доброго, чудового дня. Яскрава погода на вулиці, сонце палить, тому нашим водіям буде непереливки. Але вони не задаватимуться"
    ];
  }

  getText() {
    return this.getRandomString();
  }
}

class Racers extends Message {
  constructor(opts) {
    super(opts);
    this.stats = opts;
    this.strings = [
      "А тим часом список гонщиків: ",
      "Сьогодні в перегонах беруть участь: ",
      "Зустрічайте на старті: "
    ];
    this.string2 = "{{name}} на своїй {{car}} під номером {{index}}";
  }

  getText() {
    return (
      this.getRandomString() +
      Object.keys(this.stats)
        .map((name, index) =>
          this.string2
            .replace("{{name}}", name)
            .replace("{{car}}", this.stats[name].car)
            .replace("{{index}}", index + 1)
        )
        .join(", ")
    );
  }
}

class RaceStatus extends Message {
  constructor(opts) {
    super(opts);
    this.stats = opts;
    this.positions = [
      "нульовий",
      "перший",
      "другий",
      "третій",
      "четвертий",
      "п'ятий"
    ];
    this.string2 = "{{name}} на {{car}} зараз {{position}}";
  }

  getText() {
    return Object.keys(this.stats)
      .sort((a, b) => a.progress - b.progress)
      .map((name, index) =>
        this.string2
          .replace("{{name}}", name)
          .replace("{{car}}", this.stats[name].car)
          .replace("{{position}}", this.positions[index + 1])
      );
  }
}

class BeforeFinish extends Message {
  constructor(opts) {
    super(opts);
    this.stats = opts;
    this.strings = [
      "До фінішу залишилось зовсім небагато і схоже що першим його може перетнути {{name1}} на своєму {{car1}}. "
    ];
    this.secondPlaceString = "Друге місце може залишитись {{name2}}. ";
    this.endString = "Але давайте дочекаємось фінішу.";
  }

  getText() {
    const sortedNames = Object.keys(this.stats).sort(
      (a, b) => a.progress - b.progress
    );
    return (
      this.getRandomString()
        .replace("{{name1}}", sortedNames[0])
        .replace("{{car1}}", this.stats[sortedNames[0]].car) +
      (sortedNames[1] !== undefined
        ? this.secondPlaceString.replace("{{name2}}", sortedNames[1])
        : "") +
      this.endString
    );
  }
}

class Finish extends Message {
  constructor(opts) {
    super(opts);
    this.name = opts.name;
    this.strings = [
      "Фінішну пряму перетинає {{name}}",
      "До фінішу дістався {{name}}",
      "{{name}} вже на фініші"
    ];
  }

  getText() {
    return this.getRandomString().replace("{{name}}", this.name);
  }
}

class GameEnd extends Message {
  constructor(opts) {
    super(opts);
    this.name = Object.keys(opts).sort((a, b) => a.time - b.time)[0];
    this.time = opts[this.name].time;
    this.strings = [
      "Гонку завершено. Отож фінальний результат, перемагає {{name}}!!! Тривалість заїзду: {{time}} с",
      "Гонку завершено. Вітаємо {{name}} із перемогою!!! Тривалість заїзду: {{time}} с",
      "Гонку завершено. {{name}} перемагає в сьогоднішньому заїзді!!! Тривалість заїзду: {{time}} с"
    ];
    this.finished = _.every(opts, item => item.status == 1);
  }

  getText() {
    if (this.finished) {
      return this.getRandomString()
        .replace("{{name}}", this.name)
        .replace("{{time}}", this.time);
    } else {
      return "Ніхто так і не зміг добратися до фінішу вчасно...";
    }
  }
}

class Gibber extends Message {
  constructor(opts) {
    super(opts);
    this.strings = [
      "А ви знали, що перші перегони на справжніх автомобілях відбулися у Франції у 1894 році.",
      "Уявіть, за один заїзд учасник змагань Формула-1 втрачає два кілограми маси",
      "Сонце сховалося за хмари і здається от-от розпочнеться злива. Обережніше на поворотах.",
      "Щороку в зоопарку Індіанаполісу у США проводять перегони черепах!",
      "Зірковим гостем наших перегонів є іспанський автогонщик, дворазовий чемпіон світу у класі Формула-1 — Фернандо Алонсо",
      "Болід формули 1 може розвинути швидкість від 0 до 160 км / год за чотири секунди!",
      "Вага боліда формули 1 складає всього 500 кілограм.",
      "Без аеродинамічного спойлера управління втрачається на швидкості 180 км / год, середня швидкість гоночного боліда більше 300 км / ч.",
      "У вуличних гонках, як гран прі Монако, всі каналізаційні люки приварюють до землі, тому що їх підіймає в повітря аеродинамічна сила карта.",
      "На ПітСтоп формули 1 швидкість заправки становить 12 літрів палива в секунду. Вони використовують обладнання для заправки вертольотів.",
      "На великій швидкості, шини боліда формули 1 обертаються 50 разів на секунду і розраховані на 100-120 кілометрів.",
      "Хуан Мануель Фанхіо - найстаріший гонщик в історії формули 1, який виграв чемпіонат. Йому було 46 років, коли він переміг у 1957 році.",
      "Майк Теквелл з Нової Зеландії став наймолодшим гонщиком формули 1 в 19 років.",
      "Шумахер прийшов першим у 212 гонках.",
      "Водії болідів формули 1 піддаються високим перевантаженням і температурам під час гонки, за цей час вони втрачають до 4 кг ваги.",
      "Водії повинні знімати кермо, щоб потрапити всередину автомобіля.",
      "У дні дуже жарких гонок в таких країнах, як Австралія, Малайзія та Бразилія, водії можуть випити до 8 літрів води.",
      "Номери гонщикам присвоюються відповідно до положення кожної команди на чемпіонаті попереднього сезону. Номер 13 не видається нікому.",
      "Більшість водіїв формули 1 починають свою кар'єру в картингу."
    ];
  }

  getText() {
    return this.getRandomString();
  }
}

const messageTypes = {
  Message,
  Introduce,
  Racers,
  RaceStatus,
  BeforeFinish,
  Finish,
  GameEnd,
  Gibber
};

// ***************** //
//   Proxy pattern   //
// _________________ //

class MessageProxy {
  constructor(type, opts) {
    return new messageTypes[type](opts);
  }
}

module.exports = Commentator;
