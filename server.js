// TODO

// Facade pattern

const path = require("path");
const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const jwt = require("jsonwebtoken");
const passport = require("passport");
const bodyParser = require("body-parser");
const _ = require("lodash");

const indexRouter = require("./routes/index");
const Commentator = require("./commentator");
const users = require("./users.json");
const text = require("./text.json");

require("./passport.config");

app.use(express.static(path.join(__dirname, "public")));
app.use(passport.initialize());
app.use(bodyParser.json());

server.listen(3000);

app.use("/", indexRouter);

const startGameTimeoutDuration = 15; // in seconds - duration of pause before game starts
let startGameTimeout; // stores timeout object for start game
let endGameTimeout;
const gameDuration = 45; // duration of the race
const stats = {}; // user stats
const allClients = []; // all connected sockets
let countdownTime = false; // time when game starts ... since the UNIX epoch
let endGameTime = false; // time when game ends
let gameStarted = false; // game started flag
let textId; // id of text used for race
let statsCommentInterval;
let commentator;

io.on("connection", socket => {
  // Dont know how to create Commentator object only once
  if (!commentator) {
    commentator = new Commentator(socket);
  }
  allClients.push(socket);

  socket.on("userConnected", ({ token }) => {
    const userFromReq = jwt.decode(token);
    if (!userFromReq) {
      console.log("Cannot decode JWT. Someone is trying to hack you ;-)");
      socket.emit("authFailed");
      return false;
    }
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (!userInDB || userInDB.password !== userFromReq.password) {
      console.log("user not found");
      socket.emit("authFailed");
      return false;
    }
    if (gameStarted) {
      socket.emit("gameIsAlreadyStarted", { endGameTime });
      console.log("game started already");
      return false;
    }

    commentator.say("Introduce");
    console.log("Connected, user: " + userFromReq.login);

    // Save client name to socket object to clear data
    // in case of client's disconnection
    // Needs to be rewritten
    const i = allClients.indexOf(socket);
    allClients[i].userName = userFromReq.login;
    stats[userFromReq.login] = {
      progress: 0,
      status: 0,
      time: 0,
      car: users.find(item => item.login == userFromReq.login).car
    }; // status -1: crash, 0: race, 1: finished

    // if startGame hasnt started yet
    if (!countdownTime) {
      const now = new Date().getTime();
      countdownTime = now + startGameTimeoutDuration * 1000;
      startGameTimeout = setTimeout(() => {
        startGame();
      }, startGameTimeoutDuration * 1000);

      // Random text id for clients to preload
      // Probably better idea was to send text with websockets
      // But in this case the route /text/:id will be useless
      textId = Math.floor(Math.random() * text.length);
    }

    // refactor using only one emit
    socket.emit("youAreJoined", { stats });
    socket.emit("setTextId", { textId });
    socket.emit("timeoutUpdate", { countdownTime });

    socket.broadcast.emit("newUserJoined", { stats });
  });

  socket.on("increment", ({ token }) => {
    if (!gameStarted) {
      console.log("game is not started");
      return false;
    }
    const userName = jwt.decode(token).login;
    stats[userName].progress += 1;

    if (stats[userName].progress === text[textId].length) {
      stats[userName].status = 1; // finished
      const now = new Date().getTime();
      stats[userName].time = now - countdownTime;
    } else if (stats[userName].progress === text[textId].length - 15) {
      // 15 symbols before finish, because we have shorter tracks
      commentator.say("BeforeFinish", stats);
    }

    if (_.every(stats, item => item.status !== 0)) {
      // lodash, item status = 0 - means they are at start
      endGame();
    }

    socket.emit("updateProgress", { stats, userName });
    socket.broadcast.emit("updateProgress", { stats, userName });
  });

  socket.on("disconnect", function() {
    const { userName } = socket;
    console.log("Disconnect, user: " + userName);
    if (userName) {
      if (!gameStarted) {
        delete stats[userName];
      } else {
        if (stats[userName]) {
          stats[userName].status = -1; // status -1: crash, 0: race, 1: finished
        }
        socket.broadcast.emit("updateProgress", { stats });
      }
    }

    if (Object.keys(stats).length == 0) {
      // Stop race
      gameStarted = false;
      countdownTime = false;
      clearTimeout(startGameTimeout);
    }
    const i = allClients.indexOf(socket);
    allClients.splice(i, 1);
  });

  function startGame() {
    commentator.say("Racers", stats);

    statsCommentInterval = setInterval(() => {
      commentator.say("RaceStatus", stats);
    }, 15000);
    countdownTime = false;
    gameStarted = true;
    endGameTimeout = setTimeout(() => {
      endGame();
    }, gameDuration * 1000);
    const now = new Date().getTime();
    endGameTime = now + gameDuration * 1000;

    socket.emit("startGame", { endGameTime });
    socket.broadcast.emit("startGame", { endGameTime });
  }

  function endGame() {
    clearTimeout(endGameTimeout);
    commentator.say("GameEnd", stats);
    socket.emit("endGame");
    socket.broadcast.emit("endGame");
    gameStarted = false;
    countdownTime = false;
    clearInterval(statsCommentInterval);
    Object.keys(stats).forEach(key => delete stats[key]);
  }
});
