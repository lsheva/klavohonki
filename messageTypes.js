// Message types
const MESSAGE = "Message";
const INTRODUCE = "Introduce";
const NAME_USERS = "NameUsers";
const RACE_STATUS = "RaceStatus";
const BEFORE_FINISH = "BeforeFinish";
const ON_FINISH = "OnFinish";
const ON_GAME_END = "OnGameEnd";
const GIBBER = "Gibber";

module.exports = {
  MESSAGE,
  INTRODUCE,
  NAME_USERS,
  RACE_STATUS,
  BEFORE_FINISH,
  ON_FINISH,
  ON_GAME_END,
  GIBBER
};
