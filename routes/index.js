const express = require("express");
const path = require("path");
const router = express.Router();
const passport = require("passport");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser");

const users = require("../users.json");
const text = require("../text.json");

router.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "../views/index.html"));
});

router.get(
  "/race",
  /*passport.authenticate("jwt"),*/ (req, res) => {
    res.sendFile(path.join(__dirname, "../views/race.html"));
  }
);

router.get(
  "/text/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const respText = text[req.params.id];
    if (!respText) {
      res.status(404).send({
        message: `Text not found with id ${req.params.id}`
      });
    }
    res.send(respText);

    //res.sendFile(path.join(__dirname, "text.json"));
  }
);

router.get("/login", (req, res) => {
  res.sendFile(path.join(__dirname, "../views/login.html"));
});

router.post("/login", (req, res) => {
  const userFromReq = req.body;
  const userInDB = users.find(user => user.login === userFromReq.login);
  if (userInDB && userInDB.password === userFromReq.password) {
    const token = jwt.sign(userFromReq, "someSecret");
    res.status(200).json({ auth: true, token });
  } else {
    res.status(401).json({ auth: false });
  }
});

module.exports = router;
